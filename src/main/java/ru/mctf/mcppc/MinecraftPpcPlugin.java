package ru.mctf.mcppc;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import me.dpohvar.powernbt.PowerNBT;
import me.dpohvar.powernbt.api.NBTCompound;
import me.dpohvar.powernbt.api.NBTManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.plugin.java.JavaPlugin;
import ru.mctf.mcppc.wrapper.WrapperPlayServerSetSlot;
import ru.mctf.mcppc.wrapper.WrapperPlayServerWindowItems;

import java.util.Arrays;
import java.util.List;

public class MinecraftPpcPlugin extends JavaPlugin implements Listener {

    private static final List<String> DESCRIPTION_PAGES_RUS = Arrays.asList(
            ChatColor.BOLD + "Дневник Лютика, 14 июня 1272 г.\n\n" + ChatColor.RESET +
                    "Этот день настал. Сегодня мы украдем тот самый флаг, " +
                    "который испольует для общения реданская разведка. " +
                    "Говорят, Дийкстра забрал с собой все флаги, когда сбежал из правительства, и теперь хранит их в подвале под банями в Новиграде.",
            ChatColor.ITALIC + "Добавлено позже:\n" + ChatColor.RESET +
                    "Мы взорвали стену между канализацией и подвалом, но всё оказалось не так просто. Похоже, Дийкстра предвидел такой поворот событий, " +
                    "и зачаровал все флаги.",
            "В документах на столе было сказано, что увидеть флаги невооруженным глазом невозможно, " +
                    "что-то про \"скрытые параметры\", но мы даже не можем достать книги из сундуков! К тому же мы не знаем, в каком из них то, что нам нужно.\n" +
                    "Без помощи чародейки тут не обойтись.");

    private static final List<String> DESCRIPTION_PAGES_ENG = Arrays.asList(
            ChatColor.BOLD + "Dandelion's planner, June 14, 1272.\n\n" + ChatColor.RESET +
                    "This is the day. Today we steal the flag " +
                    "that redanian spies use. " +
                    "People say, when Dijkstra left his position in the government, he took all the flags with him, and now he stores them in the basement bellow his bathhouse.",
            ChatColor.ITALIC + "Added later:\n" + ChatColor.RESET +
                    "We blew up the wall between the basement and the sewers, but things turned out to be harder than we thought. It seems Dijkstra predicted our little robbery " +
                    "and enchanted all flags.",
            "From documents on the table, we learned that it's impossible to see flags with a bare eye, " +
                    "and something about \"hidden values\", but we can't even get the books from the chests! And even if we could, we don't know which chest contains the flag we need.\n" +
                    "We really can't do it without a witches' help.");

    @Override
    public void onEnable() {
        ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();

        Bukkit.getPluginManager().registerEvents(this, this);

        // Uncomment the following line to fill chests you open with books
        // Bukkit.getPluginManager().registerEvents(new ChestFillListener(), this);

        protocolManager.addPacketListener(
                new PacketAdapter(this, ListenerPriority.NORMAL,
                        PacketType.Play.Server.WINDOW_ITEMS, PacketType.Play.Server.SET_SLOT) {
                    @Override
                    public void onPacketSending(PacketEvent event) {
                        if (event.getPacketType() == PacketType.Play.Server.WINDOW_ITEMS) {
                            WrapperPlayServerWindowItems wrapper = new WrapperPlayServerWindowItems(event.getPacket());
                            ItemStack[] items = wrapper.getSlotData();
                            for (ItemStack item : items) {
                                if (item != null && item.getType() == Material.WRITTEN_BOOK) {
                                    processBook(item);
                                }
                            }
                            wrapper.setSlotData(items);
                        } else {
                            WrapperPlayServerSetSlot wrapper = new WrapperPlayServerSetSlot(event.getPacket());
                            ItemStack item = wrapper.getSlotData();
                            if (item != null && item.getType() == Material.WRITTEN_BOOK) {
                                processBook(item);
                                wrapper.setSlotData(item);
                            }
                        }
                    }
                });
    }

    private void processBook(ItemStack item) {
        NBTManager api = PowerNBT.getApi();
        NBTCompound nbt = api.read(item);

        if (nbt.getString("title").length() < 5) {
            nbt.put("title", "Redanian flag register №" + nbt.getString("title"));
        }

        nbt.put("author", "Sigismund Dijkstra");

        String title = nbt.getString("title");
        if (title.endsWith("№2154")) {
            nbt.put("flag", "mctf{m0");
        }

        if (title.endsWith("№516")) {
            nbt.put("flag", "ds_4re");
        }

        if (title.endsWith("№3001")) {
            nbt.put("flag", "_c001}");
        }

        api.write(item, nbt);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (!e.getWhoClicked().isOp()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (!e.getPlayer().isOp()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        p.getInventory().setItem(0, createTutorialBookRus());
        p.getInventory().setItem(1, createTutorialBookEng());
        p.getInventory().setHeldItemSlot(0);
        p.teleport(new Location(p.getWorld(), -1487, 5, 159, 90, 0));
        p.setFoodLevel(20);
    }

    @EventHandler
    public void onFoodChange(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }

    private ItemStack createTutorialBookRus() {
        ItemStack item = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta meta = ((BookMeta) item.getItemMeta());
        meta.setTitle("Дневник Лютика");
        meta.setAuthor("Лютик");
        meta.setPages(DESCRIPTION_PAGES_RUS);
        item.setItemMeta(meta);
        return item;
    }

    private ItemStack createTutorialBookEng() {
        ItemStack item = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta meta = ((BookMeta) item.getItemMeta());
        meta.setTitle("Dandelion's planner");
        meta.setAuthor("Dandelion");
        meta.setPages(DESCRIPTION_PAGES_ENG);
        item.setItemMeta(meta);
        return item;
    }

}
