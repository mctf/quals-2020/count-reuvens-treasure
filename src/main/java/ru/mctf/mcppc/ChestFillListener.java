package ru.mctf.mcppc;

import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.util.Arrays;
import java.util.List;

public class ChestFillListener implements Listener {

    private static final List<String> PAGES = Arrays.asList(
            "We're no strangers to love\n" +
                    "You know the rules and so do I\n" +
                    "A full commitment's what I'm thinking of\n" +
                    "You wouldn't get this from any other guy\n" +
                    "I just wanna tell you how I'm feeling\n" +
                    "Gotta make you understand",
            "Never gonna give you up\n" +
                    "Never gonna let you down\n" +
                    "Never gonna run around and desert you\n" +
                    "Never gonna make you cry\n" +
                    "Never gonna say goodbye\n" +
                    "Never gonna tell a lie and hurt you",
            "We've known each other for so long\n" +
                    "Your heart's been aching but you're too shy to say it\n" +
                    "Inside we both know what's been going on\n" +
                    "We know the game and we're gonna play it\n" +
                    "And if you ask me how I'm feeling\n" +
                    "Don't tell me you're too blind to see",
            "Never gonna give you up\n" +
                    "Never gonna let you down\n" +
                    "Never gonna run around and desert you\n" +
                    "Never gonna make you cry\n" +
                    "Never gonna say goodbye\n" +
                    "Never gonna tell a lie and hurt you",
            "Never gonna give you up\n" +
                    "Never gonna let you down\n" +
                    "Never gonna run around and desert you\n" +
                    "Never gonna make you cry\n" +
                    "Never gonna say goodbye\n" +
                    "Never gonna tell a lie and hurt you",
            "Never gonna give, never gonna give\n" +
                    "(Give you up)\n" +
                    "(Ooh) Never gonna give, never gonna give\n" +
                    "(Give you up)",
            "We've known each other for so long\n" +
                    "Your heart's been aching but you're too shy to say it\n" +
                    "Inside we both know what's been going on\n" +
                    "We know the game and we're gonna play it\n" +
                    "I just wanna tell you how I'm feeling\n" +
                    "Gotta make you understand",
            "Never gonna give you up\n" +
                    "Never gonna let you down\n" +
                    "Never gonna run around and desert you\n" +
                    "Never gonna make you cry\n" +
                    "Never gonna say goodbye\n" +
                    "Never gonna tell a lie and hurt you",
            "Never gonna give you up\n" +
                    "Never gonna let you down\n" +
                    "Never gonna run around and desert you\n" +
                    "Never gonna make you cry\n" +
                    "Never gonna say goodbye\n" +
                    "Never gonna tell a lie and hurt you",
            "Never gonna give you up\n" +
                    "Never gonna let you down\n" +
                    "Never gonna run around and desert you\n" +
                    "Never gonna make you cry");

    @EventHandler
    public void onInventoryOpenEvent(InventoryOpenEvent e) {
        if (e.getInventory().getHolder() instanceof DoubleChest) {
            DoubleChest holder = (DoubleChest) e.getInventory().getHolder();
            Chest leftHolder = (Chest) holder.getLeftSide();
            Chest rightHolder = (Chest) holder.getRightSide();

            int start = 1;
            fillBooks(leftHolder, start);
            fillBooks(rightHolder, start + 9 * 3);
        }
    }

    private void fillBooks(Chest chest, int start) {
        Inventory inv = chest.getBlockInventory();

        for (int i = 0; i < inv.getSize(); i++) {
            inv.setItem(i, createBook(i + start));
        }
    }

    private ItemStack createBook(int title) {
        ItemStack item = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta meta = ((BookMeta) item.getItemMeta());
        meta.setTitle(String.valueOf(title));
        meta.setAuthor("Sigismund Dijkstra");
        meta.setPages(PAGES);
        item.setItemMeta(meta);
        return item;
    }

}
